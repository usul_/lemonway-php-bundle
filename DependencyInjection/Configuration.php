<?php

namespace Lemonway\SdkBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lemonway_sdk');
        $rootNode
            ->children()
                ->scalarNode('directKitUrl')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('webKitUrl')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('login')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('password')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('lang')->defaultValue('fr')->cannotBeEmpty()->end()
                ->booleanNode('debug')->defaultFalse()->end()

            ->end()
        ;
        return $treeBuilder;
    }
}
